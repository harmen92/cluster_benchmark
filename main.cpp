#include <iostream>
#include <random>
#include <string>
#include "Clock/Clock.hpp"

int main(int argc, char* argv[])
{
    const int64_t numberOfRepeats = std::stol(argv[1]);

    std::cout << "The number of repeats is " << numberOfRepeats << '\n';

    Clock clock;

    const std::string seedString = "zaaienmaar";

    std::seed_seq seed(seedString.begin(), seedString.end());

    std::mt19937_64 generator;
    generator.seed(seed);

    const double timePointOne = clock.now();

    // First, draw numberOfRepeats gaussian random numbers and add them all together.
    double total=0.0;

    for(int64_t i=0; i<numberOfRepeats; ++i)
    {
        std::normal_distribution<double> distribution(0.0,1.0);
        total+=distribution(generator);
    }

    const double timePointTwo = clock.now();

    std::cout << "It took " << timePointTwo - timePointOne << " seconds to create the gaussian random numbers.\n";

    // Second, do simple divisions and multiplications with memory allocation (stack memory) and to make sure that standard library implementations have no influence

    uint64_t total2 = 0;
    for(int64_t i=0; i<numberOfRepeats; ++i)
    {
        // Create a certain number of 64 bit integers. Change this number until part two takes roughly the same time as part 1.
        constexpr int64_t numberOfElements=113;
        uint64_t numbers[numberOfElements] = {}; // simple array, since std::array is implementation dependent
        // inialise array
        for(int64_t j=0; j<numberOfElements; ++j)
        {
            numbers[j] = i+j;
        }
        total2 += numbers[numberOfElements-1];
    }

    const double timePointThree = clock.now();

    std::cout << "It took " << timePointThree - timePointTwo << " seconds to do the arithmetic operations with memory allocation.\n";

    // Third, do floating point operations that take a while, without memory allocation

    double total3=0.0;

    int64_t newNumberOfRepeats=numberOfRepeats/12+1;

    for(int64_t i=0; i<newNumberOfRepeats; ++i)
    {
        for(int64_t j=10; j<15; ++j)
        {
            total3+=static_cast<double>(i)/static_cast<double>(j);
            total3*=static_cast<double>(j);
            while(total3>1.0e4)
            {
                total3/=1.6182838;
            }
        }
    }

    const double timePointFour = clock.now();

    std::cout << "It took " << timePointFour - timePointThree << " seconds to do the simple arithmetic operations.\n";

    std::cout << total << ' ' << total2 << ' ' << total3 << std::endl;

    return 0;
}
