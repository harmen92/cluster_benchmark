This is a simple program to test how fast gaussian random number generation is on several machines.

### HOW TO COMPILE ###
Use the included makefile:

	make -f cluster_benchmark.cbp.mak

To clean:

	make -f cluster_benchmark.cbp.mak clean

### HOW TO RUN ###
The program uses a command line variable to set the number of Gaussian random numbers it needs to sample in the run.
The higher this number, the longer it takes.
Typically, I take around 10000000000 samples, which should take around 10 minutes to run, depending on the system.
Change the number as needed.

To run directly on the system:

	./bin/Release/cluster_benchmark 10000000000

Using PBS/torque and the added pbs script:

	qsub pbs_run_benchmark.sh -v CLA="10000000000"

